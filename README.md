# Avaliação para desenvolvedor Fullstack Seplag (Custódio Cunha Neto)

Aqui está a implementação do backend(API) da avaliação para desenvolvedor.

### Prerequisitos

 - Rodar os containers que estão no arquivo docker-compose.yaml na pasta docker-compose na raiz do projeto
 - As credenciais de acesso ao banco se encontram nos arquivos do caminho "RAIZ-DO-PROJETO/docker-compose/enviroment"
 - Rodar a migration no Package Manager Console (Apontando para o projeto  Web) com o comando abaixo:

 ```
 Update-Database
 ```

## Sobre o projeto

Este projeto foi implementado utilizando .net 6 utilizando CQRS, Clean Code e testes unitários. 
A task de verificação que roda a cada 5 minutos, pode ser verificada no dashboard do [hangfire](https://localhost:7158/hangfire/jobs/succeeded)
Além do que foi solicitado, também foi adicionado uma autenticação JWT. As credenciais se encontram abaixo:

```
{
  "username": "seplag",
  "password": "123456"
}
```

Este projeto possui 2 endpoints, são estes:
```
Post: /api/v1/Auth/login
Get: /api/v1/Security/updates
```
