using Newtonsoft.Json;
using Seplag.Assessment.Domain.Common;
using System; 
namespace Seplag.Assessment.Domain.Entities{ 

    public class SecurityUpdateValue : IEntityBase
    {
        [JsonIgnore]
        public int Id { get; private set; }
        [JsonProperty("id")]
        public string SecurityUpdateID { get; set; }
        public string Alias { get; set; }
        public string DocumentTitle { get; set; }
        public string? Severity { get; set; }
        public DateTime? InitialReleaseDate { get; set; }
        public DateTime? CurrentReleaseDate { get; set; }
        public string? CvrfUrl { get; set; }
        
        [JsonIgnore]
        public DateTime CreatedAt { get; private set; }
        [JsonIgnore]
        public DateTime? UpdatedAt { get; private set; }
    }

}