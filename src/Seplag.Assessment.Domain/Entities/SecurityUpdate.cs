using Newtonsoft.Json;
using System.Collections.Generic; 
namespace Seplag.Assessment.Domain.Entities{ 

    public class SecurityUpdate
    {
        [JsonProperty("@odata.context")]
        public string DataContext { get; set; }
        public List<SecurityUpdateValue> value { get; set; }
    }

}
