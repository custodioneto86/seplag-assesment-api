﻿using Seplag.Assessment.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Domain.Entities
{
    public class User : IEntityBase
    {
        public User( string userName, string name,
            string email, string password, DateTime createdAt)
        {
            UserName = userName;
            Name = name;
            Email = email;
            Password = password;
            CreatedAt = createdAt;
        }

        public int Id { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; private set; }
        public string UserName { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
    }
}
