using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Seplag.Assessment.API.Settings;
using Seplag.Assessment.Infrastructure;
using Seplag.Assessment.API.Extensions;
using Seplag.Assessment.Application;
using Seplag.Assessment.Infrastructure.Persistence;
using System.Text;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Seplag.Assessment.Application.Contracts.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();
builder.Services.AddControllers();

builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddApplicationServices(builder.Configuration);

builder.Services.Configure<JwtSettings>(c => builder.Configuration.GetSection("JwtSettings").Bind(c));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        JwtSettings settings = builder.Configuration.GetSection("JwtSettings").Get<JwtSettings>();
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey
          (Encoding.UTF8.GetBytes(settings.Key))
        };
    });

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "Seplag Assessment API", Version = "v1" });

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = @"JWT Authorization header using the Bearer scheme.
                    Enter 'Bearer'[space] and then your token in the text input below.
                    Example: \""Bearer 12345abcdef\""",
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                          {
                              Reference = new OpenApiReference
                              {
                                  Type = ReferenceType.SecurityScheme,
                                  Id = "Bearer"
                              }
                          },
                         new string[] {}
                    }
                });
});

builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));


builder.Services.AddHangfire(config =>
         config.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
         .UseSimpleAssemblyNameTypeSerializer()
         .UseDefaultTypeSerializer()
         .UseMemoryStorage());

builder.Services.AddHangfireServer();


var app = builder.Build();

app.UseHangfireDashboard();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Seplag Assessment API"));

}

app.MigrateDatabase<PostgresContext>((context, services) =>
{
    var logger = services.GetService<ILogger<PostgresContext>>();
    PostgresContextSeed
        .SeedAsync(context, logger)
        .Wait();
});



app.UseCors("corsapp");
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapRazorPages();
    endpoints.MapControllers();
    endpoints.MapHangfireDashboard();
});
app.MapControllers();

RecurringJob.AddOrUpdate("check-and-insert-security-updates",
    () => app.Services.CreateScope().ServiceProvider.GetService<ISecurityUpdatesService>().CheckAndInsertNewSecurityUpdates()      
    , "*/5 * * * *");

app.Run();
