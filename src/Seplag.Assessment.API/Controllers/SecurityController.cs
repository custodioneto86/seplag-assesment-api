﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates;
using Microsoft.AspNetCore.Authorization;



namespace Seplag.Assessment.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class SecurityController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SecurityController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet("updates")]
        public async Task<ActionResult<int>> GetUpdates([FromQuery] ListSecurityUpdatesQuery query)
        {
            var result = await _mediator.Send(query);

            return Ok(result);
        }
    }
}
