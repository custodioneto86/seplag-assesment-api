﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Seplag.Assessment.API.Settings;
using Seplag.Assessment.Application.Features.Users.Queries.Authenticate;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace Seplag.Assessment.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly JwtSettings _jwtSettings;

        public AuthController(IMediator mediator, IOptions<JwtSettings> jwtSettings)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _jwtSettings = jwtSettings.Value;
        }


        [HttpPost("login")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [AllowAnonymous]
        public async Task<ActionResult<int>> Login([FromBody] AuthenticateQuery query)
        {
            var result = await _mediator.Send(query);

            var jwtToken = GenerateJwtToken(result);

            result.Token = jwtToken;

            return Ok(result);
        }

        private string GenerateJwtToken(AuthenticateVM viewModel)
        {
            var claims = new[]
            {
                new Claim("id", $"{viewModel.Id}"),
                new Claim("userName", viewModel.UserName),
                new Claim(JwtRegisteredClaimNames.Name, viewModel.Name),
                new Claim(JwtRegisteredClaimNames.Email, viewModel.Email),
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var expiry = DateTime.Now.AddMinutes(120);

            var token = new JwtSecurityToken(issuer: null, audience: null,
                claims: claims, expires: expiry, signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = tokenHandler.WriteToken(token);

            return jwtToken;
        }
    }
}
