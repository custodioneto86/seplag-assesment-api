﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Contracts.Infrastructure
{
    public interface IEncrypterService
    {
        bool Verify(string text, string hash);
        string GenerateHash(string text);
    }
}
