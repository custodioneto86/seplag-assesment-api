﻿using FluentValidation;
using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Contracts.Infrastructure
{
    public interface ISecurityUpdatesService
    {
        Task<SecurityUpdate> GetAllFromAPI();

        Task<bool> CheckIfHasNew(List<string> ids);

        Task<List<SecurityUpdateValue>> GetNew(List<SecurityUpdateValue> securityUpdates);

        Task<List<SecurityUpdateValue>> GetFromId(string id);

        Task InsertMany(List<SecurityUpdateValue> securityUpdates);

        Task<List<SecurityUpdateValue>> GetAll();

        Task CheckAndInsertNewSecurityUpdates();
    }
}
