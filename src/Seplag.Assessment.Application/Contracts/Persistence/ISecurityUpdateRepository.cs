﻿using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Contracts.Persistence
{
    public interface ISecurityUpdateRepository : IAsyncRepository<SecurityUpdateValue>
    {
    }
}
