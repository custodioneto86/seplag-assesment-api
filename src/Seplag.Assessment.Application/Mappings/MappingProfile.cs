﻿using AutoMapper;
using Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates;
using Seplag.Assessment.Application.Features.Users.Queries.Authenticate;
using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, AuthenticateVM>().ReverseMap();
            CreateMap<SecurityUpdate, ListSecurityUpdatesVM>().ReverseMap();
            CreateMap<SecurityUpdateValue, SecurityUpdatesVM>().ReverseMap();
        }
    }
}
