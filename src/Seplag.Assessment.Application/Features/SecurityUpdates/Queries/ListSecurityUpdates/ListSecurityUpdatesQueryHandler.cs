using AutoMapper;
using MediatR;
using Seplag.Assessment.Application.Contracts.Infrastructure;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Application.Exceptions;
using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates
{
    public class ListSecurityUpdatesQueryHandler : IRequestHandler<ListSecurityUpdatesQuery, List<SecurityUpdatesVM>>
    {
        private readonly IMapper _mapper;
        private readonly ISecurityUpdatesService _securityUpdatesService;


        public ListSecurityUpdatesQueryHandler(IMapper mapper,
            ISecurityUpdatesService securityUpdatesService)
        {
            _securityUpdatesService = securityUpdatesService;
            _mapper = mapper;
        }

        public async Task<List<SecurityUpdatesVM>> Handle(ListSecurityUpdatesQuery request, CancellationToken cancellationToken)
        {

            var securityUpdates = await _securityUpdatesService.GetAll();

            return _mapper.Map<List<SecurityUpdatesVM>>(securityUpdates);
        }
    }
}
