﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates
{
    public class ListSecurityUpdatesQuery : IRequest<List<SecurityUpdatesVM>>
    {
    }
}
