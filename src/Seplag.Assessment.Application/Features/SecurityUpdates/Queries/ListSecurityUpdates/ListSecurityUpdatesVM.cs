﻿using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates
{
    public class ListSecurityUpdatesVM
    {
        public string DataContext { get; set; }
        public List<SecurityUpdatesVM> value { get; set; }

    }
}
