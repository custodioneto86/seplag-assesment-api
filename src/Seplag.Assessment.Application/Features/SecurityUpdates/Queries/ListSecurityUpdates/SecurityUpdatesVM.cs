﻿using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates
{
    public class SecurityUpdatesVM
    {
        public int ID { get; set; }
        public string Alias { get; set; }
        public string DocumentTitle { get; set; }
        public string Severity { get; set; }
        public DateTime InitialReleaseDate { get; set; }
        public DateTime CurrentReleaseDate { get; set; }
        public string CvrfUrl { get; set; }
    }
}
