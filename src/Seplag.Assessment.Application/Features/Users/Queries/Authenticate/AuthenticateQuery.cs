﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.Users.Queries.Authenticate
{
    public class AuthenticateQuery : IRequest<AuthenticateVM>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
