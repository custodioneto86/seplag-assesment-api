﻿using AutoMapper;
using MediatR;
using Seplag.Assessment.Application.Contracts.Infrastructure;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Application.Exceptions;
using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Features.Users.Queries.Authenticate
{
    public class AuthenticateQueryHandler : IRequestHandler<AuthenticateQuery, AuthenticateVM>
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IEncrypterService _encrypterService;


        public AuthenticateQueryHandler(IMapper mapper, IUserRepository userRepository,
            IEncrypterService encrypterService)
        {
            _userRepository = userRepository;
            _encrypterService = encrypterService;
            _mapper = mapper;
        }

        public async Task<AuthenticateVM> Handle(AuthenticateQuery request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetByUsernameAsync(request.Username);

            if (user == null) throw new NotFoundException(nameof(User), request.Username);

            if (!_encrypterService.Verify(request.Password, user.Password)) throw new NotFoundException("Invalid Password!!!", user);

            return _mapper.Map<AuthenticateVM>(user);

        }
    }
}
