﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Application.Exceptions
{
    public class InMemoryDatabaseException : Exception
    {
        public InMemoryDatabaseException()
            : base("A problem has occurred in Merory Database.")
        {
        }
    }
}
