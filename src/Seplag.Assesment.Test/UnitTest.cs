using Bogus;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Seplag.Assessment.Application.Contracts.Infrastructure;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Domain.Entities;
using Seplag.Assessment.Infrastructure.SecurityUpdates;
using System.Linq.Expressions;
using Xunit;


namespace Seplag.Assesment.Test
{
    public class AllTests
    {
        private readonly SecurityUpdatesService _service;
        private readonly Mock<IConfiguration> _configMock;
        private readonly Mock<ISecurityUpdateRepository> _securityUpdatesMock;

        public AllTests()
        {
            Mock<IConfiguration> _configMock = new Mock<IConfiguration>();
            _configMock.SetupGet(x => x[It.Is<string>(s => s == "MSRCEndpoint")]).Returns("https://api.msrc.microsoft.com/cvrf/v2.0");
            _securityUpdatesMock = new();
            _service = new SecurityUpdatesService(_configMock.Object, _securityUpdatesMock.Object);
        }


        [Fact]
        public async Task TestEmptySecurityUpdates()
        {
            // Arrange
            _securityUpdatesMock.Setup(s => s.GetAsync(It.IsAny<Expression<Func<SecurityUpdateValue, bool>>>()))
                .ReturnsAsync(new List<SecurityUpdateValue>());

            // Act
            var result = await _service.GetAll();

            // Assert     
            Assert.Empty(result);
        }

        [Fact]
        public async Task TestCheckAndInsertNewSecurityUpdates()
        {
            // Arrange
            _securityUpdatesMock.Setup(s => s.GetAsync(It.IsAny<Expression<Func<SecurityUpdateValue, bool>>>()))
                .ReturnsAsync(new List<SecurityUpdateValue> { new SecurityUpdateValue() });

            // Act

            await _service.CheckAndInsertNewSecurityUpdates();
            List<SecurityUpdateValue> result = await _service.GetAll();

            // Assert
            Assert.NotEmpty(result);
        }


    }
}