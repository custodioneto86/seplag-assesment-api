﻿using Microsoft.EntityFrameworkCore;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Domain.Entities;
using Seplag.Assessment.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Infrastructure.Repositories
{
    internal class SecurityUpdateRepository : RepositoryBase<SecurityUpdateValue>, ISecurityUpdateRepository
    {
        public SecurityUpdateRepository(PostgresContext dbContext) : base(dbContext)
        {
        }
    }
}
