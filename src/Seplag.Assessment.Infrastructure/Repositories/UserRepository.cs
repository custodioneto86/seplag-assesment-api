﻿using Microsoft.EntityFrameworkCore;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Domain.Entities;
using Seplag.Assessment.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Infrastructure.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(PostgresContext dbContext) : base(dbContext)
        {
        }

        public async Task<User> GetByUsernameAsync(string username)
        {
            var user = await _dbContext.Users
                               .Where(o => o.UserName == username)
                               .FirstOrDefaultAsync();
            return user;
        }
    }
}
