﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Seplag.Assessment.Application.Contracts.Infrastructure;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Application.Exceptions;
using Seplag.Assessment.Application.Features.SecurityUpdates.Queries.ListSecurityUpdates;
using Seplag.Assessment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Seplag.Assessment.Infrastructure.SecurityUpdates
{
    public class SecurityUpdatesService : ISecurityUpdatesService
    {
        private readonly IConfiguration _configuration;
        private readonly ISecurityUpdateRepository _securityUpdateRepository;

        public SecurityUpdatesService(IConfiguration configuration,
            ISecurityUpdateRepository securityUpdateRepository)
        {
            _configuration = configuration;
            _securityUpdateRepository = securityUpdateRepository;
        }

        public async Task<bool> CheckIfHasNew(List<string> ids)
        {
            List<SecurityUpdateValue> existingSecutyUpdates = await _securityUpdateRepository.GetAsync(su => true);

            var existingSecutyUpdatesIds = existingSecutyUpdates.Select(su => su.Alias).ToList();

            var securityUpdateValues = ids.Except(existingSecutyUpdatesIds).ToList();

            List<string> newSecurityUpdates = ids.Where(su => securityUpdateValues.Contains(su)).ToList();

            return newSecurityUpdates.Any();
        }

        public async Task<SecurityUpdate> GetAllFromAPI()
        {
            try
            {
                string url = $"{_configuration["MSRCEndpoint"]}/updates";

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Accept = "application/json";

                var response = (HttpWebResponse)request.GetResponse();


                SecurityUpdate securityUpdate = null;

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string responseBody = await reader.ReadToEndAsync();
                    reader.Close();

                    securityUpdate = JsonConvert.DeserializeObject<SecurityUpdate>(responseBody);
                }

                return securityUpdate;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<SecurityUpdateValue>> GetNew(List<SecurityUpdateValue> securityUpdates)
        {
            List<string> securityUpdateIDs = securityUpdates.Select(su => su.Alias).ToList();

            List<SecurityUpdateValue> existingSecutyUpdates = await _securityUpdateRepository.GetAsync(su => true);

            var existingSecutyUpdatesIds = existingSecutyUpdates.Select(su => su.Alias).ToList();

            var securityUpdateValues = securityUpdateIDs.Except(existingSecutyUpdatesIds).ToList();

            List<SecurityUpdateValue> newSecurityUpdates = securityUpdates.Where(su => securityUpdateValues.Contains(su.Alias)).ToList();

            return newSecurityUpdates;
        }

        public async Task<List<SecurityUpdateValue>> GetFromId(string id)
        {
            return await _securityUpdateRepository.GetAsync(su => su.SecurityUpdateID.Contains(id));
        }

        public async Task InsertMany(List<SecurityUpdateValue> securityUpdates)
        {
            foreach (var item in securityUpdates)
            {
                item.SecurityUpdateID = item.Alias;

                await _securityUpdateRepository.AddAsync(item);
            }
        }

        public async Task<List<SecurityUpdateValue>> GetAll()
        {
            var securityUpdates = await _securityUpdateRepository.GetAsync(su => true);

            return securityUpdates.OrderByDescending(su => su.Id).ToList();   
        }

        public async Task CheckAndInsertNewSecurityUpdates()
        {
            var securityUpdate = await this.GetAllFromAPI();

            if (securityUpdate == null) throw new NotFoundException(nameof(User), null);

            var securityUpdateIds = securityUpdate.value.Select(su => su.Alias).ToList();

            var hasNewSecurityUpdates = await this.CheckIfHasNew(securityUpdateIds);

            if (hasNewSecurityUpdates)
            {
                var lstNewsecurityUpdates = await this.GetNew(securityUpdate.value);

                await this.InsertMany(lstNewsecurityUpdates);
            }
        }
    }
}
