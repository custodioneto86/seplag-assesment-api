﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Seplag.Assessment.Application.Contracts.Infrastructure;
using Seplag.Assessment.Application.Contracts.Persistence;
using Seplag.Assessment.Infrastructure.Encrypter;
using Seplag.Assessment.Infrastructure.Persistence;
using Seplag.Assessment.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Seplag.Assessment.Infrastructure.SecurityUpdates;
using Seplag.Assessment.Domain.Entities;

namespace Seplag.Assessment.Infrastructure
{
    public static class InfrastructureServiceRegistration
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMemoryCache();
            services.AddDbContext<PostgresContext>(options =>
                options.UseNpgsql(configuration["ConnectionStrings:PostgresDB"], c => c.MigrationsAssembly("Seplag.Assessment.API")),
                 ServiceLifetime.Scoped,
                 ServiceLifetime.Scoped
                );

            services.AddScoped(typeof(IAsyncRepository<>), typeof(RepositoryBase<>));
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ISecurityUpdateRepository, SecurityUpdateRepository>();

            services.AddTransient<IEncrypterService, EncrypterService>();
            services.AddTransient<ISecurityUpdatesService, SecurityUpdatesService>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            return services;
        }
    }
}
