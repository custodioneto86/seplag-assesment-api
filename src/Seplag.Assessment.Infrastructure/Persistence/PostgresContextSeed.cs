﻿using Microsoft.Extensions.Logging;
using Seplag.Assessment.Domain.Entities;
using Seplag.Assessment.Infrastructure.Encrypter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Infrastructure.Persistence
{
    public class PostgresContextSeed
    {
        public static async Task SeedAsync(PostgresContext context, ILogger<PostgresContext> logger)
        {
            if (!context.Users.Any())
            {
                context.Users.AddRange(GetPreconfiguredUser());
                await context.SaveChangesAsync();
                logger.LogInformation("Seed database associated with context {DbContextName}", typeof(PostgresContext).Name);
            }
        }

        private static IEnumerable<User> GetPreconfiguredUser()
        {
            string passwordHash = new EncrypterService().GenerateHash("123456");

            return new List<User>
            {
                new User("seplag", "Seplag", "seplag@assessment.com", passwordHash, DateTime.Now )
            };
        }
    }
}
