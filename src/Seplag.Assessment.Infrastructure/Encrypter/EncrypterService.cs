﻿using Seplag.Assessment.Application.Contracts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seplag.Assessment.Infrastructure.Encrypter
{
    public class EncrypterService : IEncrypterService
    {
        public string GenerateHash(string text)
        {
            return BCrypt.Net.BCrypt.HashPassword(text);
        }

        public bool Verify(string text, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(text, hash);
        }
    }
}
